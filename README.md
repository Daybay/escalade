# Projet3 : Création d'un site communautaire autour de l’escalade


 # Introduction : Application "Escalade"

Cette application à pour but de permettre aux membres de cette communauté
d'interagir de rechercher et d'échanger un certain nombre des informations
sur les différents sites d'escalade de la région.


 # Prérequis :

* C'est une application web en Java/JEE (JDK 8)
* Packagée (WAR) avec Apache Maven
* Déployée sur un serveur Apache Tomcat 9
* Qui utilise une BDD PostgreSQL 9.x.


 # Configuration : Création de la base de donnée

* Lancez le script instal > script_creation_bdd.sql (script de création de la BDD)
* Lancez le script instal > script_insert.sql (script d'insertion du jeu de données de Démo)

Puis il vous faudra configurer l'accès à votre base de données depuis l'App web via votre IDE

* Allez dans les fichiers : src > main > resources > dao.properties
* Puis modifiez :

url : En mettant le nom de votre base de donnée
nomutilisateur : en mettant votre username
motdepasse : en mettant votre mot de passe

* Allez dans les fichiers : src > main > resources > hibernate.cfg.xml
* Puis modifiez :

hibernate.connection.url : En mettant le nom de votre base de donnée
hibernate.connection.username : en mettant votre username
hibernate.connection.password : en mettant votre mot de passe

- Il ne reste plus qu'à lancer l'application depuis votre IDE en Apache Tomcat 9 ! -

_____________________________________________________________________________________

Auteur
* Miles *

Contributeurs
* Kevin *
* Florim *