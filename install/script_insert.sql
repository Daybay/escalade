﻿delete from longueur;
delete from voie;
delete from secteur;
DELETE FROM commentaire;
DELETE FROM rel_topo_site;
delete from site;
delete from topo;
delete from public.user;

--USER
INSERT INTO public.user (id, name, email, password) VALUES 
(1,'Flow_Rider','floflo@jv.com','1234'),
(2,'Sacha','pokemon@go.net','4321'),
(3,'Ranma','un@demi.jp','2134'),
(4,'Fly','dragon@quest.org','1243'),
(5,'Sakura','card@captor.clamp','4213');


--TOPO
INSERT INTO public.topo (id, name, user_id) VALUES 
(1,'TOPO 1 (Mont St Michel)',1),
(2,'TOPO 2 (Mont Blanc)',2),
(3,'TOPO 3 (Mont Fuji)',3);

--SITE
INSERT INTO site (id, name) VALUES 
(1,'Le Mont St Michel'),
(2,'Le Mont Blanc'),
(3,'Le Mont Fuji');

--REL_TOPO_SITE
INSERT INTO rel_topo_site (id, site_id, topo_id) VALUES
(1,1,1),
(2,2,2),
(3,3,3);

--SECTEUR
INSERT INTO secteur (id, name, site_id) VALUES 
(1,'Rouge',1),
(2,'Bleu',1),
(3,'Jaune',2),
(4,'Or',2),
(5,'Argent',3),
(6,'Crystal',3);

--VOIE
INSERT INTO voie (id, name, secteur_id, cotation, hauteur, equipped, spits_number) VALUES 
(1,'Banana Spit',1,'3c',12.00,TRUE,6),
(2,'Ice Crime',1,'7b',35.00,TRUE,14),
(3,'Draco Feu',2,'5a',20.00,TRUE,10),
(4,'Star Dust',2,'6c',30.00,TRUE,12),
(5,'Crazy Diamond',3,'8b',45.00,TRUE,16),
(6,'Isaac Netero',3,'9a',60.00,FALSE,0),
(7,'Orange Road',4,'4c',15.00,TRUE,8),
(8,'Silky Road',4,'7b',37.00,TRUE,14),
(9,'Pine Apple',5,'5a',22.00,TRUE,10),
(10,'Coco Nuts',5,'8c',48.00,TRUE,16),
(11,'Sponge Bob',6,'3b',10.00,TRUE,6),
(12,'It''s Over 9000 !!!',6,'9c',65.00,FALSE,0);

--LONGUEUR
INSERT INTO longueur (id, name, voie_id, cotation) VALUES
(1,'Banana',1,'3c'),
(2,'Spit',1,'3c'),
(3,'Ice',2,'7c'),
(4,'Crime',2,'7a'),
(5,'Draco',3,'5a'),
(6,'Feu',3,'5a'),
(7,'Star',4,'6c'),
(8,'Dust',4,'6c'),
(9,'Crazy',5,'8a'),
(10,'Diamond',5,'8c'),
(11,'Isaac',6,'9a'),
(12,'Netero',6,'9a'),
(13,'Orange',7,'4c'),
(14,'Road',7,'4c'),
(15,'Silky',8,'7a'),
(16,'Road',8,'7c'),
(17,'Pine',9,'5a'),
(18,'Apple',9,'5a'),
(19,'Coco',10,'8c'),
(20,'Nuts',10,'8c'),
(21,'Sponge',11,'3a'),
(22,'Bob',11,'3c'),
(23,'It''s Over',12,'9c'),
(24,'9000 !!!',12,'9c');


--COMMENTAIRE
INSERT INTO commentaire (id, site_id, user_id, text, create_date) VALUES
(1,1,1,'C''est mon site préféré, c''est là que j''ai appris à escalader', '2018-01-13 20:29:28.603599'),
(2,2,2,'J''y suis allé l''année passée et ce site est absolument génial !', '2018-02-13 20:29:28.603599'),
(3,3,3,'Il y a des longueurs vraiment top ici, j''y vais assez régulièrement', '2017-10-21 22:19:28.603599'),
(4,1,4,'Cool ! tu as commencé à quel âge ?', '2017-12-21 20:29:28.603599'),
(5,1,2,'J''ai commencé à 14 ans =)', '2018-01-28 10:29:28.603599'),
(6,2,5,'Avec mon frère on y va pendant les prochaines vacances de pâques, as-tu des suggestions ? Quelle est ta longueur préférée ? =P', '2017-12-23 16:29:28.603599');