﻿-- Table: public."user"

-- DROP TABLE public."user";

CREATE TABLE public."user"

(

  id serial NOT NULL,

  name character varying(255) NOT NULL,

  email character varying(255) NOT NULL,

  password character varying(255) NOT NULL,

  CONSTRAINT id_primary_key PRIMARY KEY (id)

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public."user"

  OWNER TO postgres;



 -- Table: public.topo

-- DROP TABLE public.topo;

CREATE TABLE public.topo

(

id serial NOT NULL,

  name character varying(255) NOT NULL,

  user_id integer NOT NULL, -- ID du propiétaire du topo

  CONSTRAINT topo_pkey PRIMARY KEY (id),

  CONSTRAINT proprietaire_id_foreign_key FOREIGN KEY (user_id)

      REFERENCES public."user" (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION -- ID du propriétaire du topo (clé étrangère de la table user)

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.topo

  OWNER TO postgres;

COMMENT ON COLUMN public.topo.user_id IS 'ID du propiétaire du topo';

COMMENT ON CONSTRAINT proprietaire_id_foreign_key ON public.topo IS 'ID du propriétaire du topo (clé étrangère de la table user)';

-- Index: public.fki_proprietaire_id_foreign_key

-- DROP INDEX public.fki_proprietaire_id_foreign_key;

CREATE INDEX fki_proprietaire_id_foreign_key

  ON public.topo

  USING btree

  (user_id);




-- Table: public.site

-- DROP TABLE public.site;

CREATE TABLE public.site

(

id serial NOT NULL,

  name character varying(255) NOT NULL,

  CONSTRAINT site_pkey PRIMARY KEY (id)

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.site

  OWNER TO postgres;





-- Table: public.reservation

-- DROP TABLE public.reservation;

CREATE TABLE public.reservation

(

id serial NOT NULL,

  user_id integer NOT NULL,

  topo_id integer NOT NULL,

  begin_date timestamp without time zone NOT NULL,

  end_date timestamp without time zone NOT NULL,

  CONSTRAINT reservation_id_primary_key PRIMARY KEY (id),

  CONSTRAINT topo_id_foreign_key FOREIGN KEY (topo_id)

      REFERENCES public.topo (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT user_id_foreign_key FOREIGN KEY (user_id)

      REFERENCES public."user" (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.reservation

  OWNER TO postgres;

-- Index: public.fki_topo_id_foreign_key

-- DROP INDEX public.fki_topo_id_foreign_key;

CREATE INDEX fki_topo_id_foreign_key

  ON public.reservation

  USING btree

  (topo_id);

-- Index: public.fki_user_id_foreign_key

-- DROP INDEX public.fki_user_id_foreign_key;

CREATE INDEX fki_user_id_foreign_key

  ON public.reservation

  USING btree

  (user_id);




-- Table: public.rel_topo_site

-- DROP TABLE public.rel_topo_site;

CREATE TABLE public.rel_topo_site

(

id serial NOT NULL,

  site_id integer NOT NULL,

  topo_id integer NOT NULL,

  CONSTRAINT "PK_id_rel_topo_site" PRIMARY KEY (id),

  CONSTRAINT "FK_site_id_site" FOREIGN KEY (site_id)

      REFERENCES public.site (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT "FK_topo_id_topo" FOREIGN KEY (topo_id)

      REFERENCES public.topo (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.rel_topo_site

  OWNER TO postgres;

COMMENT ON TABLE public.rel_topo_site

  IS 'table de relation entre topo et site';



-- Table: public.secteur

-- DROP TABLE public.secteur;

CREATE TABLE public.secteur

(

id serial NOT NULL,

  name character varying(255) NOT NULL,

  site_id integer NOT NULL,

  CONSTRAINT secteur_pkey PRIMARY KEY (id),

  CONSTRAINT secteur_site_id_fkey FOREIGN KEY (site_id)

      REFERENCES public.site (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.secteur

  OWNER TO postgres;



-- Table: public.voie

-- DROP TABLE public.voie;

CREATE TABLE public.voie

(

id serial NOT NULL,

  name character varying(255) NOT NULL,

  secteur_id integer NOT NULL,

  cotation character varying(2) NOT NULL,

  hauteur numeric(5,2) NOT NULL,

  equipped boolean NOT NULL DEFAULT true, -- La voie est-elle équipée de "spits" ou non ?

  spits_number integer NOT NULL DEFAULT 0,

  CONSTRAINT voie_pkey PRIMARY KEY (id),

  CONSTRAINT voie_secteur_id_fkey FOREIGN KEY (secteur_id)

      REFERENCES public.secteur (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.voie

  OWNER TO postgres;

COMMENT ON COLUMN public.voie.equipped IS 'La voie est-elle équipée de "spits" ou non ?';



-- Table: public.longueur

-- DROP TABLE public.longueur;

CREATE TABLE public.longueur

(

id serial NOT NULL,

  name character varying(255) NOT NULL,

  voie_id integer,

  cotation character varying(2) NOT NULL,

  CONSTRAINT longueur_pkey PRIMARY KEY (id),

  CONSTRAINT longueur_voie_id_fkey FOREIGN KEY (voie_id)

      REFERENCES public.voie (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.longueur

  OWNER TO postgres;


-- Table: public.commentaire

-- DROP TABLE public.commentaire;

CREATE TABLE public.commentaire

(

id serial NOT NULL,

  site_id integer NOT NULL,

  user_id integer NOT NULL,

  text text NOT NULL,

  create_date timestamp without time zone,

  CONSTRAINT commentaire_id_primary_key PRIMARY KEY (id),

  CONSTRAINT site_comment_id_foreign_key FOREIGN KEY (site_id)

      REFERENCES public.site (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT user_comment_id_foreign_key FOREIGN KEY (user_id)

      REFERENCES public."user" (id) MATCH SIMPLE

      ON UPDATE NO ACTION ON DELETE NO ACTION -- identifient du commentateur

)

WITH (

  OIDS=FALSE

);

ALTER TABLE public.commentaire

  OWNER TO postgres;

COMMENT ON CONSTRAINT user_comment_id_foreign_key ON public.commentaire IS 'identifient du commentateur ';


-- Index: public.fki_site_comment_id_foreign_key

-- DROP INDEX public.fki_site_comment_id_foreign_key;

CREATE INDEX fki_site_comment_id_foreign_key

  ON public.commentaire

  USING btree

  (site_id);

-- Index: public.fki_user_comment_id_foreign_key

-- DROP INDEX public.fki_user_comment_id_foreign_key;

CREATE INDEX fki_user_comment_id_foreign_key

  ON public.commentaire

  USING btree

  (user_id);