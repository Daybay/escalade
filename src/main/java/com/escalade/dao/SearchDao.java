package com.escalade.dao;

import com.escalade.exception.DAOException;
import com.escalade.model.Site;
import com.escalade.model.Topo;
import com.escalade.model.User;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SearchDao {
    Topo getTopoById(int id);

    /* Implémentation de la méthode définie dans l'interface UtilisateurDao */
    List<Map> searchSiteByCriteria(String siteName, String secteurName, String equipped) throws DAOException;

    List<Topo> getListTopoWithRelations();

    List<Topo> getListTopoByUser(int userId);

    Site getSiteById(int siteId);

    User getUser(String email, String password) throws Exception;

    User getUserById(int id) throws Exception;

    List<Topo> getListTopoAvailablesToday() throws Exception;

    boolean isReservationByTopoAndDate(int topoId, Date from, Date to) throws Exception;
}
