package com.escalade.dao;

import com.escalade.model.*;

public interface CrudDao {

    Integer createComment(Commentaire commentaire) throws Exception;

    Integer createUser(User user) throws Exception;

    Integer createReservation(Reservation reservation) throws Exception;

    Integer createTopo(Topo topo) throws Exception;

    Integer createSite(Site site) throws Exception;

    Integer createRelTopoSite(RelTopoSite relTopoSite) throws Exception;
}
