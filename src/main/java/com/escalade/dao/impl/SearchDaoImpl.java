package com.escalade.dao.impl;

import com.escalade.dao.SearchDao;
import com.escalade.exception.DAOException;
import com.escalade.factory.DAOFactory;
import com.escalade.model.Reservation;
import com.escalade.model.Site;
import com.escalade.model.Topo;
import com.escalade.model.User;
import com.escalade.utils.DAOUtilitaire;
import com.escalade.utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SearchDaoImpl implements SearchDao {

    private DAOFactory daoFactory;

    public SearchDaoImpl() {
    }

    public SearchDaoImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }


    public Topo getTopoById(int id) {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Topo topo = (Topo) session.createQuery(
                    "SELECT t FROM Topo t " +
                            "LEFT JOIN FETCH t.relTopoSites rts " +
                            "LEFT JOIN FETCH rts.site s " +
                            "LEFT JOIN FETCH s.secteurs se " +
                            "LEFT JOIN FETCH s.commentaires com " +
                            "LEFT JOIN FETCH com.user u " +
                            "WHERE t.id = :id").setParameter("id", id).uniqueResult();

            session.getTransaction().commit();

            return topo;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /* Implémentation de la méthode définie dans l'interface UtilisateurDao */
    @Override
    public List<Map> searchSiteByCriteria(String siteName, String secteurName, String equipped) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
        /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connexion, "" +
                            "SELECT DISTINCT " +
                            "s.id AS site_id, " +
                            "s.name as site_name, " +
                            "se.id as secteur_id, " +
                            "se.name as secteur_name, " +
                            "v.name as voie_name, " +
                            "v.cotation as voie_cotation, " +
                            "v.hauteur as voie_hauteur, " +
                            "v.equipped as voie_equipped, " +
                            "v.spits_number as voie_spits_number " +
                            "" +
                            "" +
                            "FROM site s " +
                            "LEFT JOIN secteur se ON se.site_id = s.id " +
                            "LEFT JOIN voie v ON v.secteur_id = se.id " +
                            "" +
                            "WHERE UPPER(s.name) like UPPER(?) " +
                            "OR UPPER(se.name) like UPPER(?) " +
                            "OR CAST (v.equipped as varchar(5)) = ? ",
                    false,
                    DAOUtilitaire.formatLikeParameter(siteName),
                    DAOUtilitaire.formatLikeParameter(secteurName),
                    equipped);
            resultSet = preparedStatement.executeQuery();
            List<Map> listResults = DAOUtilitaire.resultSetToArrayList(resultSet);

            return listResults;

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtilitaire.fermeturesSilencieuses(resultSet, preparedStatement, connexion);
        }
    }

    @Override
    public List<Topo> getListTopoWithRelations() {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Topo> listTopo = (List<Topo>) session.createQuery("" +
                    "SELECT t FROM Topo t " +
                    "LEFT JOIN FETCH t.relTopoSites rts " +
                    "LEFT JOIN FETCH rts.site s " +
                    "LEFT JOIN FETCH s.secteurs se " +
                    "LEFT JOIN FETCH s.commentaires com " +
                    "LEFT JOIN FETCH com.user u ")
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                    .list();

            session.getTransaction().commit();
            session.close();

            return listTopo;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Topo> getListTopoByUser(int userId) {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Topo> listTopo = (List<Topo>) session.createQuery("" +
                    "SELECT t FROM Topo t " +
                    "JOIN FETCH t.user u " +
                    "WHERE u.id = :userId")
                    .setParameter("userId", userId)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                    .list();

            session.getTransaction().commit();
            session.close();

            return listTopo;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Site getSiteById(int siteId) {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Site site = (Site) session.createQuery(
                    "SELECT s FROM Site s " +
                            "LEFT JOIN FETCH s.secteurs se " +
                            "LEFT JOIN FETCH s.commentaires com " +
                            "LEFT JOIN FETCH com.user u " +
                            "WHERE s.id = :id").setParameter("id", siteId).uniqueResult();

            session.getTransaction().commit();

            return site;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getUser(String email, String password) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            User user = (User) session.createQuery(
                    "SELECT u FROM User u " +
                            "WHERE u.email = :email " +
                            "AND u.password= :password")
                    .setParameter("email", email)
                    .setParameter("password", password)
                    .uniqueResult();

            session.getTransaction().commit();

            return user;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public User getUserById(int id) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            User user = (User) session.createQuery(
                    "SELECT u FROM User u " +
                            "WHERE u.id= :id")
                    .setParameter("id", id)
                    .uniqueResult();

            session.getTransaction().commit();

            return user;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Topo> getListTopoAvailablesToday() throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Topo> listTopo = (List<Topo>) session.createQuery("" +
                    "SELECT t " +
                    " " +
                    "FROM Topo t " +
                    "WHERE t.id not in ( " +
                    "" +
                    "SELECT DISTINCT r.topo.id " +
                    " " +
                    "FROM Reservation r " +
                    " " +
                    "WHERE current_date BETWEEN r.beginDate AND r.endDate " +
                    ") ")
                    .list();

            session.getTransaction().commit();
            session.close();

            return listTopo;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public boolean isReservationByTopoAndDate(int topoId, Date from, Date to) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Reservation> reservation = (List<Reservation>) session.createQuery("" +
                    "SELECT DISTINCT r.topo.id " +
                    " " +
                    "FROM Reservation r " +
                    " " +
                    "WHERE ((r.beginDate <= :fromDate AND :fromDate <= r.endDate) " +
                    "OR (r.beginDate <= :toDate AND :toDate <= r.endDate)) " +
                    "AND r.topo.id = :topoId " +
                    " ")
                    .setParameter("fromDate", from)
                    .setParameter("toDate", to)
                    .setParameter("topoId", topoId)
                    .list();

            session.getTransaction().commit();
            session.close();

            return reservation.size() != 0;

        } catch (Exception e) {
            throw e;
        }
    }

}
