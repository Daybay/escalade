package com.escalade.dao.impl;

import com.escalade.dao.CrudDao;
import com.escalade.factory.DAOFactory;
import com.escalade.model.*;
import com.escalade.utils.HibernateUtil;
import org.hibernate.Session;

public class CrudDaoImpl implements CrudDao {

    private DAOFactory daoFactory;

    public CrudDaoImpl() {
    }

    public CrudDaoImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Integer createComment(Commentaire commentaire) throws Exception {

        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Integer id = (Integer) session.save(commentaire);
            System.out.println("Comment created => " + id);

            session.getTransaction().commit();
            session.close();

            return id;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Integer createUser(User user) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Integer id = (Integer) session.save(user);
            System.out.println("User created => " + id);

            session.getTransaction().commit();
            session.close();

            return id;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Integer createReservation(Reservation reservation) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Integer id = (Integer) session.save(reservation);
            System.out.println("Reservation created => " + id);

            session.getTransaction().commit();
            session.close();

            return id;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Integer createTopo(Topo topo) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Integer id = (Integer) session.save(topo);
            System.out.println("Topo created => " + id);

            session.getTransaction().commit();
            session.close();

            return id;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Integer createSite(Site site) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Integer id = (Integer) session.save(site);
            System.out.println("Site created => " + id);

            session.getTransaction().commit();
            session.close();

            return id;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Integer createRelTopoSite(RelTopoSite relTopoSite) throws Exception {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Integer id = (Integer) session.save(relTopoSite);
            System.out.println("RelTopoSite created => " + id);

            session.getTransaction().commit();
            session.close();

            return id;

        } catch (Exception e) {
            throw e;
        }
    }


}
