package com.escalade.model;// default package
// Generated 10 févr. 2018 23:03:56 by Hibernate Tools 3.2.2.GA


import javax.persistence.*;
import java.util.Date;

/**
 * Reservation generated by hbm2java
 */
@Entity
@Table(name = "reservation"
        , schema = "public"
)
public class Reservation implements java.io.Serializable {


    private int id;
    private Topo topo;
    private User user;
    private Date beginDate;
    private Date endDate;

    public Reservation() {
    }

    public Reservation(int id, Topo topo, User user, Date beginDate, Date endDate) {
        this.id = id;
        this.topo = topo;
        this.user = user;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "topo_id", nullable = false)
    public Topo getTopo() {
        return this.topo;
    }

    public void setTopo(Topo topo) {
        this.topo = topo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "begin_date", nullable = false, length = 29)
    public Date getBeginDate() {
        return this.beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date", nullable = false, length = 29)
    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


}


