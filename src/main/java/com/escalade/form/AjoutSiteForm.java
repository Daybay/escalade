package com.escalade.form;

import com.escalade.model.Site;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public final class AjoutSiteForm {

    private String resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }
    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }


    public Site checkSite(HttpServletRequest request ) {
        String name = getFieldValue( request, "name" );

        Site site = new Site();

        try {
            validationName( name );
        } catch ( Exception e ) {
            setError( "name", e.getMessage() );
        }

        site.setName( name );

        if ( erreurs.isEmpty() ) {
            resultat = "Succès de l'ajout du site.";
        } else {
            resultat = "Échec de l'ajout du site.";
        }

        return site;
    }

    private void validationName( String name ) throws Exception {
        if ( name == null || name.length() < 3 ) {
            throw new Exception( "Le nom du site doit contenir au moins 3 caractères." );
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setError( String field, String message ) {
        erreurs.put( field, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }

}
