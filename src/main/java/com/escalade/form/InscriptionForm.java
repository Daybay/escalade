package com.escalade.form;

import com.escalade.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public final class InscriptionForm {

    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_PASS = "password";
    private static final String FIELD_CONF = "confirmation";
    private static final String FIELD_NAME = "name";

    private String resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }
    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }


    public User signUpUser(HttpServletRequest request ) {
        String name = getFieldValue( request, FIELD_NAME );
        String email = getFieldValue( request, FIELD_EMAIL );
        String password = getFieldValue( request, FIELD_PASS );
        String confirmation = getFieldValue( request, FIELD_CONF );

        User user = new User();

        try {
            validationEmail( email );
        } catch ( Exception e ) {
            setError( FIELD_EMAIL, e.getMessage() );
        }
        user.setEmail( email );

        try {
            validationPassword( password, confirmation );
        } catch ( Exception e ) {
            setError( FIELD_PASS, e.getMessage() );
            setError( FIELD_CONF, null );
        }
        user.setPassword( password );

        try {
            validationName( name );
        } catch ( Exception e ) {
            setError( FIELD_NAME, e.getMessage() );
        }
        user.setName( name );



        if ( erreurs.isEmpty() ) {
            resultat = "Succès de l'inscription.";
        } else {
            resultat = "Échec de l'inscription.";
        }

        return user;
    }


    private void validationEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Merci de saisir une adresse mail valide." );
            }
        } else {
            throw new Exception( "Merci de saisir une adresse mail." );
        }
    }

    private void validationPassword( String motDePasse, String confirmation ) throws Exception {
        if ( motDePasse != null && confirmation != null ) {
            if ( !motDePasse.equals( confirmation ) ) {
                throw new Exception( "Les mots de passe entrés sont différents, merci de les saisir à nouveau." );
            } else if ( motDePasse.length() < 3 ) {
                throw new Exception( "Les mots de passe doivent contenir au moins 3 caractères." );
            }
        } else {
            throw new Exception( "Merci de saisir et confirmer votre mot de passe." );
        }
    }

    private void validationName( String name ) throws Exception {
        if ( name == null || name.length() < 3 ) {
            throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caractères." );
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setError( String field, String message ) {
        erreurs.put( field, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }

}
