package com.escalade.form;

import com.escalade.model.Secteur;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public final class AjoutSecteurForm {

    private static final String FIELD_NAME = "name";

    private String resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }
    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }


    public Secteur checkSecteur(HttpServletRequest request ) {
        String name = getFieldValue( request, FIELD_NAME );

        Secteur secteur = new Secteur();

        try {
            validationName( name );
        } catch ( Exception e ) {
            setError( FIELD_NAME, e.getMessage() );
        }

        secteur.setName( name );

        if ( erreurs.isEmpty() ) {
            resultat = "Succès de l'ajout du secteur.";
        } else {
            resultat = "Échec de l'ajout du secteur.";
        }

        return secteur;
    }

    private void validationName( String name ) throws Exception {
        if ( name == null || name.length() < 3 ) {
            throw new Exception( "Le nom du secteur doit contenir au moins 3 caractères." );
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setError( String field, String message ) {
        erreurs.put( field, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }

}
