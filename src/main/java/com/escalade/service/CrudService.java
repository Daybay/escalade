package com.escalade.service;

import com.escalade.dao.impl.CrudDaoImpl;
import com.escalade.dao.impl.SearchDaoImpl;
import com.escalade.model.*;

public class CrudService {

    private static CrudDaoImpl crudDaoImpl;

    public CrudService() {
        crudDaoImpl = new CrudDaoImpl();
    }

    public Integer createComment(Commentaire commentaire) throws Exception {
        return crudDaoImpl.createComment(commentaire);
    }

    public Integer createUser(User user) throws Exception {
        return crudDaoImpl.createUser(user);
    }

    public Integer createTopo(Topo topo) throws Exception {
        return crudDaoImpl.createTopo(topo);
    }

    public Integer createReservation(Reservation reservation) throws Exception {
        return crudDaoImpl.createReservation(reservation);
    }

    public Integer createSite(Site site) throws Exception {
        return crudDaoImpl.createSite(site);
    }

    public Integer createRelTopoSite(RelTopoSite relTopoSite) throws Exception {
        return crudDaoImpl.createRelTopoSite(relTopoSite);
    }
}
