package com.escalade.service;

import com.escalade.dao.impl.SearchDaoImpl;
import com.escalade.model.*;

import java.util.Date;
import java.util.List;

public class SearchService {

    private static SearchDaoImpl searchDaoImpl;

    public SearchService() {
        searchDaoImpl = new SearchDaoImpl();
    }

    public SearchDaoImpl searchDaoImpl() {
        return searchDaoImpl;
    }

    public Topo getTopo(int id) {
        return searchDaoImpl.getTopoById(id);
    }

    public List<Topo> getListTopoWithRelations() {
        return searchDaoImpl.getListTopoWithRelations();
    }

    public List<Topo> getListTopoByUser(int userId) {
        return searchDaoImpl.getListTopoByUser(userId);
    }

    public Site getSite(int siteId) {
        return searchDaoImpl.getSiteById(siteId);
    }

    public User getUser(String email, String password) throws Exception {
        return searchDaoImpl.getUser(email, password);
    }

    public User getUserById(int id) throws Exception {
        return searchDaoImpl.getUserById(id);
    }

    public List<Topo> getListTopoAvailablesToday() throws Exception {
        return searchDaoImpl.getListTopoAvailablesToday();
    }

    public boolean getReservationByTopoAndDate(int topoId, Date from, Date to) throws Exception {
        return searchDaoImpl.isReservationByTopoAndDate(topoId, from, to);
    }
}
