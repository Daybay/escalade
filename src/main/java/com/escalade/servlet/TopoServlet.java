package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.dao.SearchDao;
import com.escalade.factory.DAOFactory;
import com.escalade.model.Topo;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/topo")
public class TopoServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private SearchDao searchDao;
    private CrudDao crudDao;

    public TopoServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.searchDao = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getSearchDao();
        this.crudDao = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getCrudDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Parameter transmission
        String topoId = request.getParameter( "id" );

        // List topos with relations
        SearchService searchService = new SearchService();
        Topo topo = searchService.getTopo(Integer.parseInt(topoId));

        request.setAttribute("topo", topo);

        this.getServletContext().getRequestDispatcher("/WEB-INF/topo.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

}
