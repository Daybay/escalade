package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.dao.SearchDao;
import com.escalade.factory.DAOFactory;
import com.escalade.form.InscriptionForm;
import com.escalade.model.Topo;
import com.escalade.model.User;
import com.escalade.service.CrudService;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/inscription")
public class InscriptionServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public static final String ATT_USER = "userSignUp";
    public static final String ATT_FORM = "form";

    private CrudDao crudDao;

    public InscriptionServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.crudDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getCrudDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        InscriptionForm form = new InscriptionForm();

        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        User user = form.signUpUser(request);

//        User
//             miles@miles.fr
//                monpassword
//                pethippocrate

        if (form.getErreurs().size() == 0) {
            try {
                CrudService crudService = new CrudService();
                Integer createdUserId = crudService.createUser(user);
            } catch (Exception e) {
                form.getErreurs().put("erreur","erreur insertion");
                form.setResultat("Une erreur est survenue durant l'inscription.");
            }
        }

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute(ATT_FORM, form);
        request.setAttribute(ATT_USER, user);

        this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);

    }

}
