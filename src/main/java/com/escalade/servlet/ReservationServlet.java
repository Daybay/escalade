package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.dao.SearchDao;
import com.escalade.factory.DAOFactory;
import com.escalade.form.AjoutTopoForm;
import com.escalade.model.*;
import com.escalade.service.CrudService;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@WebServlet("/reservation")
public class ReservationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private SearchDao searchDao;
    private CrudDao crudDao;

    public ReservationServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.searchDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getSearchDao();
        this.crudDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getCrudDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // List topos with relations
        SearchService searchService = new SearchService();
        List<Topo> listTopos = null;
        try {
            listTopos = searchService.getListTopoAvailablesToday();
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("errorReservation", "");
        request.setAttribute("successReservation", "");
        request.setAttribute("listTopos", listTopos);

        this.getServletContext().getRequestDispatcher("/WEB-INF/reservation.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        // List topos with relations
        SearchService searchService = new SearchService();
        List<Topo> listTopos = null;
        try {
            listTopos = searchService.getListTopoAvailablesToday();
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("listTopos", listTopos);


        // Récupération des paramètres **********************************
        String topoId = request.getParameter("topoId");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        request.setAttribute("errorReservation", "");

        if (topoId == null || from == null || to == null) {
            request.setAttribute("errorReservation", "Veuillez saisir tous les champs");
            this.getServletContext().getRequestDispatcher("/WEB-INF/reservation.jsp").forward(request, response);
            return;
        }

        try {
            // Check si topo pas déjà réservé sur cette période  **********************************
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date fromDate = format.parse(from);
            Date toDate = format.parse(to);
            boolean isReserved = searchService.getReservationByTopoAndDate(Integer.parseInt(topoId), fromDate, toDate);

            if (!isReserved) {
                Reservation reservation = new Reservation();

            /* Récupération de la session depuis la requête */
                HttpSession session = request.getSession();
                User userSession = (User) session.getAttribute(ConnectionServlet.ATT_SESSION_USER);
                User userDatas = searchService.getUserById(userSession.getId());
                reservation.setUser(userDatas);

                Topo topo = new Topo();
                topo.setId(Integer.parseInt(topoId));
                reservation.setTopo(topo);
                reservation.setBeginDate(format.parse(from));
                reservation.setEndDate(format.parse(to));

                CrudService crudService = new CrudService();
                Integer createdReservationId = crudService.createReservation(reservation);
                request.setAttribute("successReservation", "La réservation a bien été effectuée");

            } else {
                request.setAttribute("errorReservation", "Ce Topo est déjà réervé à cette date. Veuillez en sélectionner une autre.");
            }

        } catch (Exception e) {
            request.setAttribute("errorReservation", "Erreur lors de la réservation");
            e.printStackTrace();
        }

//        request.setAttribute("siteId", siteId);
//        request.setAttribute("reservation", reser);

        this.getServletContext().getRequestDispatcher("/WEB-INF/reservation.jsp").forward(request, response);
    }

}
