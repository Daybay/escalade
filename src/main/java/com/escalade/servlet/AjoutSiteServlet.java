package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.factory.DAOFactory;
import com.escalade.form.AjoutSiteForm;
import com.escalade.model.RelTopoSite;
import com.escalade.model.Site;
import com.escalade.model.Topo;
import com.escalade.model.User;
import com.escalade.service.CrudService;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/ajoutSite")
public class AjoutSiteServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private CrudDao crudDao;

    public AjoutSiteServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.crudDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getCrudDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Topo> listTopos = null;
        try {

            SearchService searchService = new SearchService();
                /* Récupération de la session depuis la requête */
            HttpSession session = request.getSession();
            User userSession = (User) session.getAttribute(ConnectionServlet.ATT_SESSION_USER);
            User userDatas = searchService.getUserById(userSession.getId());

            listTopos = searchService.getListTopoByUser(userDatas.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("listTopos", listTopos);

        /* Affichage de la page d'ajout de site */
        this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutSite.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Topo> listTopos = null;
        try {

            SearchService searchService = new SearchService();
                /* Récupération de la session depuis la requête */
            HttpSession session = request.getSession();
            User userSession = (User) session.getAttribute(ConnectionServlet.ATT_SESSION_USER);
            User userDatas = searchService.getUserById(userSession.getId());

            listTopos = searchService.getListTopoByUser(userDatas.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("listTopos", listTopos);

        String topoId = request.getParameter("topoId");

        /* Préparation de l'objet formulaire */
        AjoutSiteForm form = new AjoutSiteForm();

        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        Site site = form.checkSite(request);

        if (form.getErreurs().size() == 0) {
            try {

                CrudService crudService = new CrudService();
                Integer createdSiteId = crudService.createSite(site);

                if (topoId != null && !topoId.isEmpty()) {
                    Topo topo = new Topo();
                    topo.setId(Integer.parseInt(topoId));
                    RelTopoSite relTopoSite = new RelTopoSite();
                    relTopoSite.setSite(site);
                    relTopoSite.setTopo(topo);
                    Integer createdRelTopoSiteId = crudService.createRelTopoSite(relTopoSite);
                }

            } catch (Exception e) {
                form.getErreurs().put("erreur", "erreur insertion");
                form.setResultat("Une erreur est survenue durant l'ajout du site.");
            }
        }

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute("form", form);
        request.setAttribute("siteToAdd", site);

        this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutSite.jsp").forward(request, response);

    }

}
