package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.dao.SearchDao;
import com.escalade.factory.DAOFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/search")
public class SearchServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private SearchDao searchDao;
    private CrudDao crudDao;

    public SearchServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.searchDao = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getSearchDao();
        this.crudDao = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getCrudDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("siteName", "");
        request.setAttribute("secteurName", "");
        request.setAttribute("equipped", false);

        this.getServletContext().getRequestDispatcher("/WEB-INF/search.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String siteName = request.getParameter("siteName");
        String secteurName = request.getParameter("secteurName");

        String equipped = request.getParameter("equipped");

        // Multi-search infos
        List<Map> listSites = searchDao.searchSiteByCriteria(siteName, secteurName, equipped);

        request.setAttribute("siteName", siteName);
        request.setAttribute("secteurName", secteurName);
        request.setAttribute("equipped", equipped);

        request.setAttribute("listSites", listSites);

        this.getServletContext().getRequestDispatcher("/WEB-INF/search.jsp").forward(request, response);
    }

}
