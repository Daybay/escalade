package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.factory.DAOFactory;
import com.escalade.form.AjoutTopoForm;
import com.escalade.model.Topo;
import com.escalade.model.User;
import com.escalade.service.CrudService;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/ajoutTopo")
public class AjoutTopoServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public static final String ATT_TOPO = "topoToAdd";
    public static final String ATT_FORM = "form";

    private CrudDao crudDao;

    public AjoutTopoServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.crudDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getCrudDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutTopo.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        AjoutTopoForm form = new AjoutTopoForm();

        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        Topo topo = form.checkTopo(request);

        if (form.getErreurs().size() == 0) {
            try {

                SearchService searchService = new SearchService();
                /* Récupération de la session depuis la requête */
                HttpSession session = request.getSession();
                User userSession = (User) session.getAttribute(ConnectionServlet.ATT_SESSION_USER);
                User userDatas = searchService.getUserById(userSession.getId());

                topo.setUser(userDatas);

                CrudService crudService = new CrudService();
                Integer createdTopoId = crudService.createTopo(topo);
            } catch (Exception e) {
                e.printStackTrace();
                form.getErreurs().put("erreur", "erreur insertion");
                form.setResultat("Une erreur est survenue durant l'ajout du topo.");
            }
        }

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute(ATT_FORM, form);
        request.setAttribute(ATT_TOPO, topo);

        this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutTopo.jsp").forward(request, response);

    }

}
