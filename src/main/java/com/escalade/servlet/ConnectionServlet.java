package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.dao.SearchDao;
import com.escalade.factory.DAOFactory;
import com.escalade.form.ConnectionForm;
import com.escalade.model.Topo;
import com.escalade.model.User;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/connection")
public class ConnectionServlet extends HttpServlet {
    public static final String ATT_USER = "userConnected";
    public static final String ATT_FORM = "form";
    public static final String ATT_SESSION_USER = "userSession";
    public static final String VUE = "/WEB-INF/connection.jsp";

    private SearchDao searchDao;
    private CrudDao crudDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.searchDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getSearchDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Affichage de la page de connexion */
        this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        ConnectionForm form = new ConnectionForm();

        /* Traitement de la requête et récupération du bean en résultant */
        User user = form.connectUser(request);

        // List topos with relations
        SearchService searchService = new SearchService();
        User user1 = null;
        try {
            user1 = searchService.getUser(user.getEmail(), user.getPassword());

            if (user1 != null) {
                user1.setPassword(null);

            /* Récupération de la session depuis la requête */
                HttpSession session = request.getSession();


                /**
                 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
                 * Utilisateur à la session, sinon suppression du bean de la session.
                 */
                if (form.getErreurs().isEmpty()) {
                    session.setAttribute(ATT_SESSION_USER, user1);
                } else {
                    session.setAttribute(ATT_SESSION_USER, null);
                }
            } else {
                form.getErreurs().put("erreur", "erreur connexion");
                form.setResultat("Identifiant ou mot de passe incorrects.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            form.getErreurs().put("erreur", "erreur connexion");
            form.setResultat("Une erreur est survenue durant la connexion.");
        }

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute(ATT_FORM, form);
        request.setAttribute(ATT_USER, user1);

        this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
    }
}