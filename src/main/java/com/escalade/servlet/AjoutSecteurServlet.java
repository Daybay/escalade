package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.factory.DAOFactory;
import com.escalade.form.AjoutSecteurForm;
import com.escalade.model.Secteur;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ajoutSecteur")
public class AjoutSecteurServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public static final String ATT_SECTEUR = "secteurToAdd";
    public static final String ATT_FORM = "form";

    private CrudDao crudDao;

    public AjoutSecteurServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.crudDao = ((DAOFactory) getServletContext().getAttribute("daofactory")).getCrudDao();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Affichage de la page d'ajout de secteur */
        this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutSecteur.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        AjoutSecteurForm form = new AjoutSecteurForm();

        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        Secteur secteur = form.checkSecteur(request);

        if (form.getErreurs().size() == 0) {
            try {
               // CrudService crudService = new CrudService();
               // Integer createdSecteurId = crudService.createSecteur(secteur);
            } catch (Exception e) {
                form.getErreurs().put("erreur","erreur insertion");
                form.setResultat("Une erreur est survenue durant l'ajout du secteur.");
            }
        }

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute(ATT_FORM, form);
        request.setAttribute(ATT_SECTEUR, secteur);

        this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutSecteur.jsp").forward(request, response);

    }

}
