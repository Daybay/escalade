package com.escalade.servlet;

import com.escalade.dao.CrudDao;
import com.escalade.dao.SearchDao;
import com.escalade.factory.DAOFactory;
import com.escalade.model.Commentaire;
import com.escalade.model.Site;
import com.escalade.model.User;
import com.escalade.service.CrudService;
import com.escalade.service.SearchService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private SearchDao searchDao;
    private CrudDao crudDao;

    public CommentServlet() {
        super();
    }

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.searchDao = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getSearchDao();
        this.crudDao = ( (DAOFactory) getServletContext().getAttribute( "daofactory" ) ).getCrudDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Parameter transmission
        String siteId = request.getParameter( "siteId" );

        request.setAttribute("siteId", siteId);
        request.setAttribute("comment", "");
        request.setAttribute("errorComment", "");
        request.setAttribute("successComment", "");

        this.getServletContext().getRequestDispatcher("/WEB-INF/comment.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Parameter transmission
        String siteId = request.getParameter( "siteId" );
        String comment = request.getParameter( "comment" );

        request.setAttribute("errorComment", "");

        if (comment == null || comment.length() < 1 || comment.length() > 2000) {
            request.setAttribute("errorComment", "Le commentaire doit faire entre 1 et 2000 caractères");
            this.getServletContext().getRequestDispatcher("/WEB-INF/comment.jsp").forward(request, response);
            return;
        }

        // List topos with relations
//        SearchService searchService = new SearchService();
//        Site site = searchService.getSite(Integer.parseInt(topoId));

        Commentaire commentaire = new Commentaire();
        commentaire.setText(comment);
        commentaire.setCreateDate(new Date());

        Site site = new Site();
        site.setId(Integer.parseInt(siteId));

        commentaire.setSite(site);


        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
        User userSession = (User) session.getAttribute(ConnectionServlet.ATT_SESSION_USER);

        // List topos with relations
        SearchService searchService = new SearchService();
        User userDatas = null;
        try {
            userDatas = searchService.getUserById(userSession.getId());

            User user = new User();
            user.setId(user.getId());
            commentaire.setUser(userDatas);

            CrudService crudService = new CrudService();
            try {
                Integer createdCommId = crudService.createComment(commentaire);
                request.setAttribute("successComment", "Le commentaire à bien été ajouté");
            } catch (Exception e) {
                request.setAttribute("errorComment", "Le commentaire n'a pas été ajouté, veuillez réessayer.");
            }
        } catch (Exception e) {
            request.setAttribute("errorComment", "Le commentaire n'a pas été ajouté, veuillez réessayer.");
        }

        request.setAttribute("siteId", siteId);
        request.setAttribute("comment", comment);

        this.getServletContext().getRequestDispatcher("/WEB-INF/comment.jsp").forward(request, response);

    }

}
