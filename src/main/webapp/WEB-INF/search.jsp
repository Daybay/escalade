<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Rechercher</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>

<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

    <p>Page Search !</p>

    <form action="search" method="POST" enctype="application/x-www-form-urlencoded">
        Libellé du site : <input type="text" name="siteName" value="<%= request.getAttribute("siteName")%>"
                                 placeholder="Nom du site..."/>
        <br/>
        OU
        <br/>
        Secteur recherché : <input type="text" name="secteurName" value="<%= request.getAttribute("secteurName")%>"
                                   placeholder="Secteur recherché..."/>
        <br/>
        OU
        <br/>
        Équipé :
        <input type="radio" name="equipped" value="true"/>Oui
        <input type="radio" name="equipped" value="false"/>Non
        <br/>
        <input type="submit" value="Rechercher"/>

        <div style="margin: 30px;">

            <%
                List<Map> listSites = (List<Map>) request.getAttribute("listSites");
            %>

            <c:if test="${listSites.size() == 0}">
                Aucun résultat ne correspond à votre recherche.
            </c:if>

            <c:if test="${listSites.size() != 0}">
                <table style="width:30%">
                    <tr>
                        <th>Site</th>
                        <th>Secteur</th>
                        <th>Voie</th>
                        <th>Cotation</th>
                        <th>Équipée</th>
                        <th>Hauteur</th>
                        <th>Spits</th>
                    </tr>
                    <c:forEach items="${listSites}" var="site">

                        <tr style="text-align: center">
                            <td><c:out value="${site.site_name}"/></td>
                            <td><c:out value="${site.secteur_name}"/></td>
                            <td><c:out value="${site.voie_name}"/></td>
                            <td><c:out value="${site.voie_cotation}"/></td>
                            <td><c:out value="${site.voie_equipped}"/></td>
                            <td><c:out value="${site.voie_hauteur}"/></td>
                            <td><c:out value="${site.voie_spits_number}"/></td>
                        </tr>

                        <%--<tr>--%>
                        <%--Nom: <c:out value="${site}"/><br/>--%>
                        <%--<br/>--%>
                        <%--</tr>--%>
                    </c:forEach>
                </table>
            </c:if>

        </div>

    </form>

    <%@include file="includes/footer.jsp" %>

</div>
</body>
</html>
