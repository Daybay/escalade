<%@ page import="com.escalade.model.Topo" %>
<%@ page import="java.util.List" %>
<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Réservations</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>

<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

    <c:if test="${!empty sessionScope.userSession}">
        <%
            List<Topo> listTopos = (List<Topo>) request.getAttribute("listTopos");
        %>

        <form action="reservation" method="POST" enctype="application/x-www-form-urlencoded">
            <c:forEach items="${listTopos}" var="topo">

                <label for="radioTopo<c:out value="${topo.id}"/>"><input id="radioTopo<c:out value="${topo.id}"/>"
                                                                         type="radio" name="topoId"
                                                                         value="<c:out value="${topo.id}"/>"/> <c:out
                        value="${topo.name}"/> </label>
                <br/><br/>
                <%--<div>--%>
                <%--<h2>- <c:out value="${topo.name}"/> (<a href="/topo?id=${topo.id}">Réserver</a>) </h2>--%>
                <%--</div>--%>
            </c:forEach>

            <br/><br/>
            <label for="from">De</label>
            <input type="text" id="from" name="from">
            <br/><br/>
            <label for="to">Jusqu'au</label>
            <input type="text" id="to" name="to">
            <br/><br/>
            <input type="submit" value="Réserver"/>

            <br/><br/>

            <div style="color: red;">
                <%= request.getAttribute("successReservation")%>
            </div>

            <div style="color: green;">
                <%= request.getAttribute("errorReservation")%>
            </div>


        </form>

    </c:if>

    <c:if test="${empty sessionScope.userSession}">
        Vous devez être connecté pour réserver un topo.
    </c:if>

    <%@include file="includes/footer.jsp" %>
</div>
</body>
</html>
