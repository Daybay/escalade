<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>

<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

    <p>Bienvenue sur le site d'Escalade. Ce site vous permet de consulter, partager, réserver et commenter
        les divers sites disponibles.</p>

    <br/><br/>
    <%@include file="includes/footer.jsp" %>

</div>
</body>
</html>
