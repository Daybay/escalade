<%@ page import="com.escalade.model.Topo" %>
<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Topo</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>

<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

    <p>Page Search !</p>


    <%
        Topo topo = (Topo) request.getAttribute("topo");
    %>

    <div>
        <h2><c:out value="${topo.name}"/> </h2>
        <br/>

        <c:forEach items="${topo.relTopoSites}" var="relTopoSite">
            <h3>
                Site : <c:out value="${relTopoSite.site.name}"/>
            </h3>
            <br/>

            <c:forEach items="${relTopoSite.site.commentaires}" var="comm">
                <div class="comment">

                    <div>
                        <u>Posté le : <i> <fmt:formatDate type = "both" dateStyle = "medium" timeStyle = "medium" value = "${comm.createDate}" /></u> (par : <strong><c:out value="${comm.user.name}"/>)</strong></i><br/>
                        <p><c:out value="${comm.text}"/><br/></p>
                    </div>
                </div>
            </c:forEach>


            <c:if test="${empty sessionScope.userSession}">
                Vous devez vous connecter pour ajouter un commentaire : <a href="connection">Se connecter</a><br/>
            </c:if>
            <c:if test="${!empty sessionScope.userSession}">
                <a href="comment?siteId=${relTopoSite.site.id}">Ajouter un commentaire</a><br/>
            </c:if>

            <c:forEach items="${relTopoSite.site.secteurs}" var="secteur">
                ----Secteur: <c:out value="${secteur.name}"/><br/>
            </c:forEach>
        </c:forEach>
        <br/>
    </div>


    <%@include file="includes/footer.jsp" %>

</div>
</body>
</html>
