<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Inscription</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>
<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>
    <fieldset>

        <c:if test="${not (empty form.erreurs and not empty form.resultat)}">

            <form method="post" action="inscription">

                <legend>Inscription</legend>
                <p>Vous pouvez vous inscrire via ce formulaire.</p>

                    <%--Email--%>
                <label for="email">Adresse email <span class="requis">*</span></label>
                <input type="email" id="email" name="email" value="<c:out value="${userSignUp.email}"/>" size="20"
                       maxlength="60"/>
                <span class="erreur">${form.erreurs['email']}</span>
                <br/>

                    <%--password--%>
                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="motdepasse" name="password" value="" size="20" maxlength="20"/>
                <span class="erreur">${form.erreurs['password']}</span>
                <br/>

                    <%--password confirmation--%>
                <label for="confirmation">Confirmation du mot de passe <span class="requis">*</span></label>
                <input type="password" id="confirmation" name="confirmation" value="" size="20" maxlength="20"/>
                <span class="erreur">${form.erreurs['confirmation']}</span>
                <br/>

                    <%--name--%>
                <label for="nom">Nom d'utilisateur <span class="requis">*</span></label>
                <input type="text" id="nom" name="name" value="<c:out value="${userSignUp.name}"/>" size="20"
                       maxlength="20"/>
                <span class="erreur">${form.erreurs['name']}</span>
                <br/>

                <input type="submit" value="Inscription" class="sansLabel"/>
                <br/>

            </form>
        </c:if>

        <p class="${empty form.erreurs ? 'succes' : 'erreur'}">
            ${form.resultat} <br/><br/>
            <a href="<c:url value="/home"/>">Retourner à l'accueil</a>
        </p>
    </fieldset>

    <%@include file="includes/footer.jsp" %>
</div>
</body>
</html>