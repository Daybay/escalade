<%@ page import="com.escalade.model.Topo" %>
<%@ page import="java.util.List" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Ajout Site</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>
<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

    <c:if test="${!empty sessionScope.userSession}">

        <fieldset>

            <c:if test="${not (empty form.erreurs and not empty form.resultat)}">

                <%
                    List<Topo> listTopos = (List<Topo>) request.getAttribute("listTopos");
                %>

                <form method="post" action="ajoutSite" enctype="application/x-www-form-urlencoded">

                    <legend>Ajout site</legend>
                    <p>Vous pouvez ajouter un site via ce formulaire.</p>

                    <br/>
                    Liste des topos :<br/>
                    <c:forEach items="${listTopos}" var="topo">
                        <label for="radioTopo<c:out value="${topo.id}"/>"><input
                                id="radioTopo<c:out value="${topo.id}"/>" type="radio" name="topoId"
                                value="<c:out value="${topo.id}"/>"/> <c:out value="${topo.name}"/> </label>
                        <br/><br/>
                    </c:forEach>

                    <br/><br/>

                        <%--name--%>
                    <label for="nom">Nom du site <span class="requis">*</span></label>
                    <input type="text" id="nom" name="name" value="<c:out value="${siteToAdd.name}"/>" size="20"
                           maxlength="20"/>
                    <span class="erreur">${form.erreurs['name']}</span>
                    <br/>

                    <input type="submit" value="Ajouter" class="sansLabel"/>
                    <br/>

                </form>
            </c:if>

            <p class="${empty form.erreurs ? 'succes' : 'erreur'}">
                    ${form.resultat} <br/><br/>
                <a href="<c:url value="/home"/>">Retourner à l'accueil</a>
            </p>
        </fieldset>

    </c:if>

    <c:if test="${empty sessionScope.userSession}">
        Vous devez être connectés pour ajouter un site.
    </c:if>

    <%@include file="includes/footer.jsp" %>
</div>
</body>
</html>