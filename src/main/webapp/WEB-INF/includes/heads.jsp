<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
<link type="text/css" rel="stylesheet" href="../assets/css/form.css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<%--<link rel="stylesheet" href="/resources/demos/style.css">--%>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#datepicker").datepicker(
            {
                minDate: +1,
                maxDate: "+1M +10D"
            }
        );
    });


    $(function () {
        var dateFormat = "mm/dd/yy",
            // *********************************************************
            from = $("#from")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3,
                    minDate: +1,
                    maxDate: "+1M +10D"
                })
                .on("change", function () {
                    var fromDate = getDate(this);
                    var toDate = fromDate;
                    toDate.setDate(toDate.getDate() + 3);
//                    from.datepicker("option", "minDate", fromDate);
                    to.datepicker("option", "maxDate", toDate);
                })
            ,
            // *********************************************************
            to = $("#to").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 2,
                minDate: +1,
                maxDate: "+1M +10D"
            })
                .on("change", function () {
                    var toDate = getDate(this);
                    console.log($("#from").datepicker);
                    var fromDate = toDate;
                    fromDate.setDate(fromDate.getDate() - 3);
//                    to.datepicker("option", "minDate", fromDate);
//                    from.datepicker("option", "maxDate", toDate);
                });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
    });
</script>