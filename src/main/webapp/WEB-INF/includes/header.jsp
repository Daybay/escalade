<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
    </div>
</nav>

<ul>
    <li><a href="<c:url value="/home"/>">Accueil</a></li>
    <li><a href="<c:url value="/search"/>">Rechercher</a></li>
    <li><a href="<c:url value="/topos"/>">Topos</a></li>
    <c:if test="${!empty sessionScope.userSession}">
        <li><a href="<c:url value="/ajoutTopo"/>">Ajouter un topo</a></li>
        <li><a href="<c:url value="/ajoutSite"/>">Ajouter un site</a></li>
        <li><a href="<c:url value="/reservation"/>">Réservation</a></li>
    </c:if>

    <li><a href="<c:url value="/inscription"/>">Inscription</a></li>
</ul>

<br/><br/>

<ul>
    <c:if test="${empty sessionScope.userSession}">
        <li><a href="<c:url value="/connection"/>">Connexion</a></li>
    </c:if>
    <c:if test="${!empty sessionScope.userSession}">
        <li><a href="<c:url value="/deconnection"/>">Deconnexion</a></li>
    </c:if>
</ul>

