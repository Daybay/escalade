<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Ajout Secteur</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>
<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>
    <fieldset>

        <c:if test="${not (empty form.erreurs and not empty form.resultat)}">

            <form method="post" action="ajoutSecteur">

                <legend>Ajout secteur</legend>
                <p>Vous pouvez ajouter un secteur via ce formulaire.</p>

                    <%--name--%>
                <label for="nom">Nom du secteur <span class="requis">*</span></label>
                <input type="text" id="nom" name="name" value="<c:out value="${secteurToAdd.name}"/>" size="20"
                       maxlength="20"/>
                <span class="erreur">${form.erreurs['name']}</span>
                <br/>

                <input type="submit" value="Ajouter" class="sansLabel"/>
                <br/>

            </form>
        </c:if>

        <p class="${empty form.erreurs ? 'succes' : 'erreur'}">
            ${form.resultat} <br/><br/>
            <a href="<c:url value="/home"/>">Retourner à l'accueil</a>
        </p>
    </fieldset>

    <%@include file="includes/footer.jsp" %>
</div>
</body>
</html>