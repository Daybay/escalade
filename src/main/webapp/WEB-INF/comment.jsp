<%@ page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Commentaire</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>

<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

    <c:if test="${!empty sessionScope.userSession}">

        <form action="comment?siteId=<%= request.getAttribute("siteId")%>" method="POST"
              enctype="application/x-www-form-urlencoded">
            Ecrivez votre commentaire :
            <textarea rows="4" cols="50" name="comment"
                      placeholder="Veuillez saisir votre commentaire..."><%= request.getAttribute("comment")%></textarea>


            <div style="color: red;">
                <%= request.getAttribute("errorComment")%>
            </div>

            <div style="color: green;">
                <%= request.getAttribute("successComment")%>
            </div>


            <br/>
            <input type="submit" value="Poster"/>

        </form>

    </c:if>

    <c:if test="${empty sessionScope.userSession}">
        Vous devez être connecté pour ajouter un commentaire.
    </c:if>

    <%@include file="includes/footer.jsp" %>

</div>
</body>
</html>
