<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Connexion</title>
    <%@include file="includes/heads.jsp" %>
</head>
<body>
<div class="page">

    <%-- header and nabar includes  --%>
    <%@include file="includes/header.jsp" %>
    <%@include file="includes/navbar.jsp" %>

        <form method="post" action="connection">
            <fieldset>
                <legend>Connexion</legend>
                <p>Vous pouvez vous connecter via ce formulaire.</p>

                <label for="email">Adresse email <span class="requis">*</span></label>
                <input type="email" id="email" name="email" value="<c:out value="${userConnected.email}"/>" size="20" maxlength="60" />
                <span class="erreur">${form.erreurs['email']}</span>

                <br/>

                <label for="password">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="password" name="password" value="" size="20" maxlength="20" />
                <span class="erreur">${form.erreurs['motdepasse']}</span>

                <br/>

                <input type="submit" value="Connexion" class="sansLabel" />


                <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>

                <%-- Vérification de la présence d'un objet utilisateur en session --%>
                <c:if test="${!empty sessionScope.userSession}">
                    <%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>
                    <p class="succes">Vous êtes connecté(e) avec l'adresse : ${sessionScope.userSession.email}</p>
                </c:if>
            </fieldset>
        </form>

    <%@include file="includes/footer.jsp" %>

</div>
</body>
</html>